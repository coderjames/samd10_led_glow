/*****************************************************************************
 * Copyright (c) 2018 Rowley Associates Limited.                             *
 *                                                                           *
 * This file may be distributed under the terms of the License Agreement     *
 * provided with this software.                                              *
 *                                                                           *
 * THIS FILE IS PROVIDED AS IS WITH NO WARRANTY OF ANY KIND, INCLUDING THE   *
 * WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. *
 *                                                                           *
 *****************************************************************************/

/*****************************************************************************
 *                         Preprocessor Definitions                          *
 *                         ------------------------                          *
 * STARTUP_FROM_RESET                                                        *
 *                                                                           *
 *   If defined, the program will startup from power-on/reset. If not        *
 *   defined the program will just loop endlessly from power-on/reset.       *
 *                                                                           *
 * VECTORS_IN_RAM                                                            *
 *                                                                           *
 *   If defined, an area of RAM will large enough to store the vector table  *
 *   will be reserved.                                                       *
 *                                                                           *
 *****************************************************************************/

  .syntax unified
  .code 16

  .section .init, "ax"
  .align 0

/*****************************************************************************
 * Default Exception Handlers                                                *
 *****************************************************************************/

#ifndef STARTUP_FROM_RESET

  .thumb_func
  .weak Reset_Wait
Reset_Wait:
  b .

#endif

  .thumb_func
  .weak NMI_Handler
NMI_Handler:
  b .

  .thumb_func
  .weak HardFault_Handler
HardFault_Handler:
  b .

  .thumb_func
  .weak SVC_Handler
SVC_Handler:
  b .

  .thumb_func
  .weak PendSV_Handler
PendSV_Handler:
  b .

  .thumb_func
  .weak SysTick_Handler
SysTick_Handler:
  b .

  .thumb_func
Dummy_Handler:
  b .

#if defined(__OPTIMIZATION_SMALL)

  .weak PM_Handler
  .thumb_set PM_Handler,Dummy_Handler

  .weak SYSCTRL_Handler
  .thumb_set SYSCTRL_Handler,Dummy_Handler

  .weak WDT_Handler
  .thumb_set WDT_Handler,Dummy_Handler

  .weak RTC_Handler
  .thumb_set RTC_Handler,Dummy_Handler

  .weak EIC_Handler
  .thumb_set EIC_Handler,Dummy_Handler

  .weak NVMCTRL_Handler
  .thumb_set NVMCTRL_Handler,Dummy_Handler

  .weak DMAC_Handler
  .thumb_set DMAC_Handler,Dummy_Handler

  .weak EVSYS_Handler
  .thumb_set EVSYS_Handler,Dummy_Handler

  .weak SERCOM0_Handler
  .thumb_set SERCOM0_Handler,Dummy_Handler

  .weak SERCOM1_Handler
  .thumb_set SERCOM1_Handler,Dummy_Handler

  .weak SERCOM2_Handler
  .thumb_set SERCOM2_Handler,Dummy_Handler

  .weak TCC0_Handler
  .thumb_set TCC0_Handler,Dummy_Handler

  .weak TC1_Handler
  .thumb_set TC1_Handler,Dummy_Handler

  .weak TC2_Handler
  .thumb_set TC2_Handler,Dummy_Handler

  .weak ADC_Handler
  .thumb_set ADC_Handler,Dummy_Handler

  .weak AC_Handler
  .thumb_set AC_Handler,Dummy_Handler

  .weak DAC_Handler
  .thumb_set DAC_Handler,Dummy_Handler

#else

  .thumb_func
  .weak PM_Handler
PM_Handler:
  b .

  .thumb_func
  .weak SYSCTRL_Handler
SYSCTRL_Handler:
  b .

  .thumb_func
  .weak WDT_Handler
WDT_Handler:
  b .

  .thumb_func
  .weak RTC_Handler
RTC_Handler:
  b .

  .thumb_func
  .weak EIC_Handler
EIC_Handler:
  b .

  .thumb_func
  .weak NVMCTRL_Handler
NVMCTRL_Handler:
  b .

  .thumb_func
  .weak DMAC_Handler
DMAC_Handler:
  b .

  .thumb_func
  .weak EVSYS_Handler
EVSYS_Handler:
  b .

  .thumb_func
  .weak SERCOM0_Handler
SERCOM0_Handler:
  b .

  .thumb_func
  .weak SERCOM1_Handler
SERCOM1_Handler:
  b .

  .thumb_func
  .weak SERCOM2_Handler
SERCOM2_Handler:
  b .

  .thumb_func
  .weak TCC0_Handler
TCC0_Handler:
  b .

  .thumb_func
  .weak TC1_Handler
TC1_Handler:
  b .

  .thumb_func
  .weak TC2_Handler
TC2_Handler:
  b .

  .thumb_func
  .weak ADC_Handler
ADC_Handler:
  b .

  .thumb_func
  .weak AC_Handler
AC_Handler:
  b .

  .thumb_func
  .weak DAC_Handler
DAC_Handler:
  b .

#endif

/*****************************************************************************
 * Vector Table                                                              *
 *****************************************************************************/

  .section .vectors, "ax"
  .align 0
  .global _vectors
  .extern __stack_end__
#ifdef STARTUP_FROM_RESET
  .extern Reset_Handler
#endif

_vectors:
  .word __stack_end__
#ifdef STARTUP_FROM_RESET
  .word Reset_Handler
#else
  .word Reset_Wait
#endif
  .word NMI_Handler
  .word HardFault_Handler
  .word 0 /* Reserved */
  .word 0 /* Reserved */
  .word 0 /* Reserved */
  .word 0 /* Reserved */
  .word 0 /* Reserved */
  .word 0 /* Reserved */
  .word 0 /* Reserved */
  .word SVC_Handler
  .word 0 /* Reserved */
  .word 0 /* Reserved */
  .word PendSV_Handler
  .word SysTick_Handler
  .word PM_Handler
  .word SYSCTRL_Handler
  .word WDT_Handler
  .word RTC_Handler
  .word EIC_Handler
  .word NVMCTRL_Handler
  .word DMAC_Handler
  .word Dummy_Handler /* Reserved */
  .word EVSYS_Handler
  .word SERCOM0_Handler
  .word SERCOM1_Handler
  .word SERCOM2_Handler
  .word TCC0_Handler
  .word TC1_Handler
  .word TC2_Handler
  .word ADC_Handler
  .word AC_Handler
  .word DAC_Handler
_vectors_end:

#ifdef VECTORS_IN_RAM
  .section .vectors_ram, "ax"
  .align 0
  .global _vectors_ram

_vectors_ram:
  .space _vectors_end - _vectors, 0
#endif
