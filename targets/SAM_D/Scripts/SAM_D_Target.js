/*****************************************************************************
 * Copyright (c) 2015 Rowley Associates Limited.                             *
 *                                                                           *
 * This file may be distributed under the terms of the License Agreement     *
 * provided with this software.                                              *
 *                                                                           *
 * THIS FILE IS PROVIDED AS IS WITH NO WARRANTY OF ANY KIND, INCLUDING THE   *
 * WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. *
 *                                                                           *
 *****************************************************************************/

function Reset()
{
  // Reset the target
  TargetInterface.pokeWord(0xE000EDFC, 0x01000000); // DEMCR - Disable vector catch and enable DWT
  TargetInterface.pokeWord(0xE0001020, TargetInterface.peekWord(0x00000004) & 0xFFFFFFFE); // DWT_COMP0
  TargetInterface.pokeWord(0xE0001024, 0x00000000); // DWT_MASK0
  TargetInterface.pokeWord(0xE0001028, 0x00000004); // DWT_FUNCTION0
  TargetInterface.pokeWord(0xE000ED0C, 0x05FA0004); // AIRCR - System reset request
  TargetInterface.pokeWord(0xE000EDF0, 0xA05F0003); // DHCSR - Halt
  TargetInterface.waitForDebugState(1000);
  TargetInterface.peekWord(0xE0001028); // DWT_FUNCTION0 - Clear match
  TargetInterface.pokeWord(0xE0001024, 0x00000000); // DWT_MASK0
  TargetInterface.pokeWord(0xE0001028, 0x00000000); // DWT_FUNCTION0
}

function GetPartName()
{
  // Return the part name
  var did = TargetInterface.peekWord(0x41002018);
  var processor = (did >> 28) & 0xF
  var family = (did >> 23) & 0x1F;
  var series = (did >> 16) & 0x3F;
  var devsel = did & 0xFF;
  if (family == 0)
    {
      if (series == 0)
        {
          switch (devsel)
            {
              case 0x00:
                return "ATSAMD20J18";
              case 0x01:
                return "ATSAMD20J17";
              case 0x02:
                return "ATSAMD20J16";
              case 0x03:
                return "ATSAMD20J15";
              case 0x04:
                return "ATSAMD20J14";

              case 0x05:
                return "ATSAMD20G18";
              case 0x06:
                return "ATSAMD20G17";
              case 0x07:
                return "ATSAMD20G16";
              case 0x08:
                return "ATSAMD20G15";
              case 0x09:
                return "ATSAMD20G14";

              case 0x0A:
                return "ATSAMD20E18";
              case 0x0B:
                return "ATSAMD20E17";
              case 0x0C:
                return "ATSAMD20E16";
              case 0x0D:
                return "ATSAMD20E15";
              case 0x0E:
                return "ATSAMD20E14";
            }
        }
      else if (series == 1)
        {
          switch (devsel)
            {
              case 0x00:
                return "ATSAMD21J18A";
              case 0x01:
                return "ATSAMD21J17A";
              case 0x02:
                return "ATSAMD21J16A";
              case 0x03:
                return "ATSAMD21J15A";
              case 0x04:
                return "ATSAMD21J14A";

              case 0x05:
                return "ATSAMD21G18A";
              case 0x06:
                return "ATSAMD21G17A";
              case 0x07:
                return "ATSAMD21G16A";
              case 0x08:
                return "ATSAMD21G15A";
              case 0x09:
                return "ATSAMD21G14A";

              case 0x0A:
                return "ATSAMD21E18A";
              case 0x0B:
                return "ATSAMD21E17A";
              case 0x0C:
                return "ATSAMD21E16A";
              case 0x0D:
                return "ATSAMD21E15A";
              case 0x0E:
                return "ATSAMD21E14A";

              case 0x29:
                return "ATSAMDA1J16A";
              case 0x2A:
                return "ATSAMDA1J15A";
              case 0x2B:
                return "ATSAMDA1J14A";
              case 0x2C:
                return "ATSAMDA1G16A";
              case 0x2D:
                return "ATSAMDA1G15A";
              case 0x2E:
                return "ATSAMDA1G14A";
              case 0x2F:
                return "ATSAMDA1E16A";
              case 0x30:
                return "ATSAMDA1E15A";
              case 0x31:
                return "ATSAMDA1E14A";

              case 0x40:
                return "ATSAMDA0J16A";
              case 0x41:
                return "ATSAMDA0J15A";
              case 0x42:
                return "ATSAMDA0J14A";
              case 0x43:
                return "ATSAMDA0G16A";
              case 0x44:
                return "ATSAMDA0G15A";
              case 0x45:
                return "ATSAMDA0G14A";
              case 0x46:
                return "ATSAMDA0E16A";
              case 0x47:
                return "ATSAMDA0E15A";
              case 0x48:
                return "ATSAMDA0E14A";
            }
        }
      else if (series == 2)
        {
          switch (devsel)
            {
              case 0x00:
                return "ATSAMD10D14AM";
              case 0x01:
                return "ATSAMD10D13AM";
              case 0x03:
                return "ATSAMD10D14ASS";
              case 0x04:
                return "ATSAMD10D13ASS";
              case 0x06:
                return "ATSAMD10C14A/ATSAMD10D14A";
              case 0x07:
                return "ATSAMD10C13A/ATSAMD10D13A";           
            }
        }
      else if (series == 3)
        {
          switch (devsel)
            {
              case 0x00:
                return "ATSAMD11D14AM";
              case 0x03:
                return "ATSAMD11D14ASS";
              case 0x06:
                return "ATSAMD11C14A/ATSAMD11D14A";
              case 0x07:
                return "ATSAMD11C13A/ATSAMD11D13A";
            }
        }
      else if (series == 4)
        {
          switch (devsel)
            {
              case 0x00:
                return "ATSAMD09D14A";
              case 0x07:
                return "ATSAMD09C13A";
            }
        }
    }

  return "";
}

function MatchPartName(name)
{
  var partName = GetPartName();

  if (partName == "")
    return false;

  return partName.indexOf(name) != -1;
}

function EnableTrace(traceInterfaceType)
{
}

