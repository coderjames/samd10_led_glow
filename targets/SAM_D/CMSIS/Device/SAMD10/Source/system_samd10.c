/**
 * \file
 *
 * \brief Low-level initialization functions called upon chip startup.
 *
 * Copyright (c) 2015 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 * 03/12/2015 - Modified by Rowley Associates to configure OSC8M to run at
 *              8MHz.
 *
 */

#include "samd10.h"

#define __SYSTEM_CLOCK    (8000000)

uint32_t SystemCoreClock = __SYSTEM_CLOCK;/*!< System Clock Frequency (Core Clock)*/

/**
 * Initialize the system
 *
 * @brief  Setup the microcontroller system.
 *         Initialize the System and update the SystemCoreClock variable.
 */
void SystemInit(void)
{
	SYSCTRL_OSC8M_Type osc8m;

	NVMCTRL->CTRLB.bit.RWS = 1; // needs 1 once we switch to 48 MHz

    // Internal 8 MHz oscillator with /8 prescaler is default core
    // clock at boot. Turn off the /8 prescaler to run it at 8 MHz.
	osc8m = SYSCTRL->OSC8M;
	osc8m.bit.PRESC = 0;
	osc8m.bit.ONDEMAND = 1;
	osc8m.bit.RUNSTDBY = 0;
	SYSCTRL->OSC8M = osc8m;

	SystemCoreClock = __SYSTEM_CLOCK;

    /* End goal of the below:
        Use DFLL @ 48 MHz as System Core Clock.
            Use DFLL as input to GCLK_MAIN (generic clock generator #0)
    */

    /* SAMD21 errata:
        DFLL on_demand bit in DFLLCTRL MUST be set to a zero before configuring the DFLL module,
        otherwise writing to the DFLL register can freeze the device.

        Assume this applies to the D10 also.
    */
    SYSCTRL->DFLLCTRL.reg = 0x0000;

    /* recover the factory calibration values from NVM */
    SYSCTRL_DFLLVAL_Type dfllval;
    uint32_t coarse = REG_ACCESS(uint32_t, FUSES_DFLL48M_COARSE_CAL_ADDR);
    coarse &= FUSES_DFLL48M_COARSE_CAL_Msk;
    coarse >>= FUSES_DFLL48M_COARSE_CAL_Pos;
    dfllval.bit.COARSE = coarse;

    uint32_t fine = REG_ACCESS(uint32_t, FUSES_DFLL48M_FINE_CAL_ADDR);
    fine &= FUSES_DFLL48M_FINE_CAL_Msk;
    fine >>= FUSES_DFLL48M_FINE_CAL_Pos;
    dfllval.bit.FINE = fine;
    SYSCTRL->DFLLVAL = dfllval;
    
    /* turn on the DFLL */
    SYSCTRL_DFLLCTRL_Type dfllctrl;
    dfllctrl.reg = 0;
    dfllctrl.bit.ENABLE = 1;
    SYSCTRL->DFLLCTRL = dfllctrl;

    /* switch Generator #0 (GCLK_MAIN) source from OSC8M to DFLL */
    GCLK_GENCTRL_Type genctrl;
    genctrl.reg = 0x00010600; // reset value for generator #0 (enabled, OSC8M as source) */
    genctrl.bit.SRC = GCLK_GENCTRL_SRC_DFLL48M_Val;
    GCLK->GENCTRL = genctrl;

    SystemCoreClock = 48000000; // 48 MHz, more or less

	return;
}

/**
 * Update SystemCoreClock variable
 *
 * @brief  Updates the SystemCoreClock with current core Clock
 *         retrieved from cpu registers.
 */
void SystemCoreClockUpdate(void)
{
	if (SYSCTRL->DFLLCTRL.bit.ENABLE == 1)
        SystemCoreClock = 48000000;
    else
        SystemCoreClock = __SYSTEM_CLOCK;

	return;
}
