//
// main.cpp : Defines entry point for an SAM D C/C++ application.
//

#include <sam.h>

// LED0 on D10 Xplained-mini is connected to PORT A pin 09
// configure PA09 as push-pull output
static const int LED_PORT = 0;
static const int LED_PIN  = 9;

// enable only one of these modes at a time
#define LED_BLINK
#undef LED_BLINK

#define LED_GLOW
//#undef LED_GLOW

#if !defined(LED_BLINK) && !defined(LED_GLOW)
#error "No LED mode defined"
#endif

#if defined(LED_BLINK) && defined(LED_GLOW)
#error "Can only define one LED mode at a time"
#endif

#if defined(LED_GLOW)
// for a PWM frequency of 1,024 Hz
#define TCC0_PERIOD_COUNT (46874)

// set these to the same value to pulse at 1 Hz
// set the duty cycle update faster than the step
//     size changes in order to get a faster glow
#define DUTY_CYCLE_UPDATES_PER_SECOND (256)
#define TCC0_STEP_SIZE (TCC0_PERIOD_COUNT / 256)
#endif

void Configure_LED(void)
{
    // set pin as an output regardless of blink or glow mode
    PORT->Group[LED_PORT].DIRSET.reg = (1 << LED_PIN);

#if defined(LED_GLOW)
    // PA09 is also TCC0 Waveform-Output #3 when
    // it is configured in the "E" alternate-function
    // mode (TC/TCC).
    PORT->Group[LED_PORT].PINCFG[LED_PIN].bit.PMUXEN = 1;
    PORT->Group[LED_PORT].PMUX[LED_PIN/2].bit.PMUXO = PORT_PMUX_PMUXO_E_Val;
#endif
}

#if defined(LED_GLOW)
void Configure_TCC(void)
{
    // configure TCC0 to use clock generator 0 (i.e. DFLL)
    // as its APB input clock
    GCLK_CLKCTRL_Type clkctrl;
    clkctrl.reg = 0x0000;
    clkctrl.bit.ID = GCLK_CLKCTRL_ID_TCC0_Val;
    clkctrl.bit.GEN = GCLK_CLKCTRL_GEN_GCLK0_Val;
    clkctrl.bit.CLKEN = 1;
    GCLK->CLKCTRL.reg = clkctrl.reg;

    // and unmask the TCC0 bus clock in the Power Manager
    PM->APBCMASK.bit.TCC0_ = 1;

    // TCC0 input clock is 48 MHz
    // want it to output a PWM at Waveform-Output #3
    // 1 kHz PWM frequency, starting at a 50% duty cycle

    // "In general, the term 'timer' is used when the timer/count clock
    //  control is handled by an internal source, and the term 'counter'
    //  is used when the clock control is handled externally. (e.g.
    //  counting external events)"
    //
    // "The counter value is also compared to the CCx registers.
    //  The waveform generator modes use these comparison to set
    //  the waveform period or pulse width."

    // enable up-counting from 0 to TOP (PER register)
    //TCC0->CTRLBCLR.reg = TCC_CTRLBCLR_DIR;
    // disable dubble buffering of the registers during initialization
    TCC0->CTRLBSET.reg = TCC_CTRLBSET_LUPD;
    // select single-slope PWM generation mode
    TCC0->WAVE.bit.WAVEGEN = TCC_WAVE_WAVEGEN_NPWM_Val;
    
    // PWM frequency = F_gclk / (N*(TOP+1))
    // F_gclk = 48,000,000 Hz
    // N = prescaler divider = 1
    // TOP = PER register value
    //
    // set both the counter registers and their buffer copies to the same value
    // to start with. After initialization, update locking will be disabled so
    // that the TC0 interrupt can update the TCC0->CCB[3] register which will get
    // copied to the TCC0->CC[3] register on timer overflow
    TCC0->PER.reg = TCC0_PERIOD_COUNT; 
    TCC0->PERB.reg = TCC0_PERIOD_COUNT;

    TCC0->CC[3].reg = TCC0_PERIOD_COUNT / 8;
    TCC0->CCB[3].reg = TCC0_PERIOD_COUNT / 8;

    // configure the prescaler and enable the TCC (CTRLA.ENABLE)
    TCC_CTRLA_Type ctrla;
    ctrla.reg = 0;
    ctrla.bit.ENABLE = 1;
    ctrla.bit.PRESCALER = TCC_CTRLA_PRESCALER_DIV1_Val;
    ctrla.bit.PRESCSYNC = TCC_CTRLA_PRESCSYNC_PRESC_Val;

    TCC0->CTRLA.reg = ctrla.reg;

    TCC0->CTRLBCLR.reg = TCC_CTRLBCLR_LUPD;
}
#endif

void Configure_TCs(void)
{
    // configure TC1 & TC2 to use clock generator 0 (i.e. DFLL)
    // as their APB input clock
    GCLK_CLKCTRL_Type clkctrl;
    clkctrl.reg = 0x0000;
    clkctrl.bit.ID = GCLK_CLKCTRL_ID_TC1_TC2_Val;
    clkctrl.bit.GEN = GCLK_CLKCTRL_GEN_GCLK0_Val;
    clkctrl.bit.CLKEN = 1;
    GCLK->CLKCTRL.reg = clkctrl.reg;

    // and unmask the TC1 bus clock in the Power Manager
    PM->APBCMASK.bit.TC1_ = 1;

    // TC1 input clock should be 48 MHz
    // want it to expire at 1 Hz and count down from the load value to zero
    TC1->COUNT16.CTRLBSET.reg = TC_CTRLBSET_DIR;  // count down from CC[0] to 0
    TC1->COUNT16.DBGCTRL.reg = TC_DBGCTRL_DBGRUN; // run TC1 even while core halted
#if defined(LED_BLINK)
    TC1->COUNT16.CC[0].reg = 46875; // 48 MHz / 1024 = 46,875 counts per second
#endif
#if defined(LED_GLOW)
    // for glow effect, update the PWM duty cycle periodically
    // 48 MHz / 1024 = 46,875 counts per second
    TC1->COUNT16.CC[0].reg = 46875 / DUTY_CYCLE_UPDATES_PER_SECOND;
#endif
    TC_CTRLA_Type ctrla;
    ctrla.reg = 0;
    ctrla.bit.ENABLE = 1;
    ctrla.bit.MODE = TC_CTRLA_MODE_COUNT16_Val;
    ctrla.bit.PRESCALER = TC_CTRLA_PRESCALER_DIV1024_Val;
    ctrla.bit.PRESCSYNC = TC_CTRLA_PRESCSYNC_GCLK_Val;
    ctrla.bit.WAVEGEN = TC_CTRLA_WAVEGEN_MFRQ_Val;
    TC1->COUNT16.CTRLA.reg = ctrla.reg;

    // Generate an interrupt on overflow/compare
    REG_TC1_INTENSET = TC_INTENSET_OVF;
}

void TC1_Handler(void)
{
#if defined(LED_BLINK)
    // toggle the LED at the TC1 counting rate
    PORT->Group[LED_PORT].OUTTGL.reg = (1 << LED_PIN);
#endif
#if defined(LED_GLOW)
    static int32_t tcc_count = 0;
    static int32_t tcc_step = TCC0_STEP_SIZE;

    // update the TCC0 PWM duty cycle in a sinusoidal pattern
    // to create a glowing effect
    TCC0->CCB[3].reg = tcc_count;
    
    // start with a dumb fixed-size increment that
    // wraps in a saw-tooth pattern to see if this
    // actually works to change the duty cycle of TCC0
    tcc_count += tcc_step;
    if (tcc_count > TCC0_PERIOD_COUNT)
    {
        tcc_step = -TCC0_STEP_SIZE;
        tcc_count = TCC0_PERIOD_COUNT;
    }
    else if( tcc_count < 0 )
    {
        tcc_step = TCC0_STEP_SIZE;
        tcc_count = 0;
    }
#endif

    // W1C to ack the overflow
    REG_TC1_INTFLAG = TC_INTFLAG_OVF;
}


int main(int argc, char *argv[])
{
    Configure_LED();
    Configure_TCs();
#if defined(LED_GLOW)
    Configure_TCC();
#endif

    // Enable TC1 interrupt recognition in the NVIC
    __NVIC_EnableIRQ(TC1_IRQn);
    // Enable IRQ recognition in the M0+ core
    __enable_irq();

    while(1)
    {
        __WFI();
    }

    return 0;
}
