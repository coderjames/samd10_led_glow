/*****************************************************************************
 * Copyright (c) 2018 Rowley Associates Limited.                             *
 *                                                                           *
 * This file may be distributed under the terms of the License Agreement     *
 * provided with this software.                                              *
 *                                                                           *
 * THIS FILE IS PROVIDED AS IS WITH NO WARRANTY OF ANY KIND, INCLUDING THE   *
 * WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. *
 *                                                                           *
 *****************************************************************************/

#ifndef __sam_h
#define __sam_h

#if defined(__SAMD09C13A__) || defined(__SAMD09D14A__)

#include "samd09.h"

#elif defined(__SAMD10C13A__) || defined(__SAMD10C14A__) || defined(__SAMD10D13A__) || defined(__SAMD10D14A__)

#include "samd10.h"

#elif defined(__SAMD11C13A__) || defined(__SAMD11C14A__) || defined(__SAMD11D14AM__) || defined(__SAMD11D14AS__)

#include "samd11.h"

#elif defined(__SAMD20E14__) || defined(__SAMD20E15__) || defined(__SAMD20E16__) || defined(__SAMD20E17__) || defined(__SAMD20E18__) || defined(__SAMD20G14__) || defined(__SAMD20G15__) || defined(__SAMD20G16__) || defined(__SAMD20G17__) || defined(__SAMD20G18__) || defined(__SAMD20J14__) || defined(__SAMD20J15__) || defined(__SAMD20J16__) || defined(__SAMD20J17__) || defined(__SAMD20J18__)

#include "samd20.h"

#elif defined(__SAMD21E15A__) || defined(__SAMD21E15B__) || defined(__SAMD21E15BU__) || defined(__SAMD21E16A__) || defined(__SAMD21E16B__) || defined(__SAMD21E16BU__) || defined(__SAMD21E17A__) || defined(__SAMD21E18A__) || defined(__SAMD21G15A__) || defined(__SAMD21G15B__) || defined(__SAMD21G16A__) || defined(__SAMD21G16B__) || defined(__SAMD21G17A__) || defined(__SAMD21G17AU__) || defined(__SAMD21G18A__) || defined(__SAMD21G18AU__) || defined(__SAMD21J15A__) || defined(__SAMD21J15B__) || defined(__SAMD21J16A__) || defined(__SAMD21J16B__) || defined(__SAMD21J17A__) || defined(__SAMD21J18A__)

#include "samd21.h"

#elif defined(__SAMDA0E14A__) || defined(__SAMDA0E15A__) || defined(__SAMDA0E16A__) || defined(__SAMDA0G14A__) || defined(__SAMDA0G15A__) || defined(__SAMDA0G16A__) || defined(__SAMDA0J14A__) || defined(__SAMDA0J15A__) || defined(__SAMDA0J16A__)

#include "samda0.h"

#elif defined(__SAMDA1E14A__) || defined(__SAMDA1E14B__) || defined(__SAMDA1J15A__) || defined(__SAMDA1J14A__) || defined(__SAMDA1E15B__) || defined(__SAMDA1J15B__) || defined(__SAMDA1J14B__) || defined(__SAMDA1G15A__) || defined(__SAMDA1E15A__) || defined(__SAMDA1J16A__) || defined(__SAMDA1G15B__) || defined(__SAMDA1G14B__) || defined(__SAMDA1G14A__) || defined(__SAMDA1G16A__) || defined(__SAMDA1G16B__) || defined(__SAMDA1J16B__) || defined(__SAMDA1E16B__) || defined(__SAMDA1E16A__)

#include "samda1.h"

#endif

#endif
